package main

import (
	"fmt"
)

func main() {
	
	fmt.Println("🖐️ Hello World from Go 🌍")
	fmt.Println("🚧 this is a work in progress")

	<-make(chan bool)

}
